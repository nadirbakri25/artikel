<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\KategoriController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::prefix('artikel')->group(function() {
    Route::get('/', [ArticleController::class, 'index']);
    Route::get('/create', [ArticleController::class, 'create']);
    Route::get('{id}', [ArticleController::class, 'show']);
    Route::get('{id}/edit', [ArticleController::class, 'edit']);
    Route::get('/addKategori/{id}', [ArticleController::class, 'addKategori']);
    
    Route::post('/', [ArticleController::class, 'store']);
    Route::post('/addKategori', [ArticleController::class, 'storeKategori']);
    Route::post('/cari', [ArticleController::class, 'cari']);
    
    Route::put('/{id}', [ArticleController::class, 'update']);
    
    Route::delete('/{id}', [ArticleController::class, 'destroy']);
});

Route::prefix('kategori')->group(function() {
    Route::get('/', [KategoriController::class, 'index']);
    Route::get('/create', [KategoriController::class, 'create']);
    Route::get('{id}', [KategoriController::class, 'show']);
    Route::get('{id}/edit', [KategoriController::class, 'edit']);
    Route::get('/filter/{id}', [KategoriController::class, 'filter']);
    
    Route::post('/', [KategoriController::class, 'store']);
    
    Route::put('/{id}', [KategoriController::class, 'update']);
    
    Route::delete('/{id}', [KategoriController::class, 'destroy']);
});