@extends('layout/main')

@section('judul', 'Tambah Data')

@section('content')
<nav aria-label="breadcrumb" class="mt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/artikel">Home</a></li>
      <li class="breadcrumb-item"><a href="/artikel">Artikel</a></li>
      <li class="breadcrumb-item active" aria-current="page">Edit Artikel</li>
    </ol>
</nav>

<form method="POST" action="/artikel/{{ $artikel->id }}">
    @csrf
    @method('PUT')

    <input type="hidden" class="form-control" id="inputId" name="id" value="{{ $artikel->i }}">

    <div class="mb-3">
        <label for="inputJudul" class="form-label">Judul</label>
        <input type="text" class="form-control" id="inputJudul" name="judul" value="{{ $artikel->judul }}">
        @error('judul')
            <p class="text-danger">{{ $message }}</p>
        @enderror
    </div>

    <div class="mb-3">
        <label for="inputKonten" class="form-label">Konten</label>
        <textarea type="text" class="form-control" id="inputKonten" name="konten">
            {!! $artikel->konten !!}
        </textarea>
        @error('konten')
            <p class="text-danger">{{ $message }}</p>
        @enderror
    </div>
    
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

<script src="https://cdn.tiny.cloud/1/mbahydy1hfdkwvd117g1ylg0f5qtmh4eah51nwgce7e1gqoy/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
    tinymce.init({
        selector: 'textarea',
        plugins: 'advlist autolink lists link image charmap print preview hr anchor pagebreak',
        toolbar_mode: 'floating',
   });
</script>
@endsection