@extends('layout/main')

@section('judul', 'Tambah Kategori Artikel')

@section('content')
<nav aria-label="breadcrumb" class="mt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/artikel">Home</a></li>
      <li class="breadcrumb-item"><a href="/artikel">Artikel</a></li>
      <li class="breadcrumb-item active" aria-current="page">Tambah Kategori Artikel</li>
    </ol>
</nav>

<form method="POST" action="/artikel/addKategori">
    @csrf

    <input type="hidden" name="id" value="{{ $artikel->id }}">

    <div class="mb-3">
        <label for="inputKategori" class="form-label">Tambah Kategori</label>
        <select id="inputKategori" class="form-select mb-3" name="category_id">
            <option selected value="0">Klik untuk membuka</option>
            @foreach ($kategoriExclude as $item)
                <option value={{ $item->id }}>{{ $item->nama }}</option>
            @endforeach
        </select>
        @error('category_id')
            <p class="text-danger">{{ $message }}</p>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection