@extends('layout/main')

@section('judul', 'Home')

@section('content')
<nav aria-label="breadcrumb" class="mt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/artikel">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Artikel</li>
    </ol>
</nav>

<div class="card">
    <div class="card-header">
      <div class="row align-items-center">
          <div class="col-10">
              <span>Artikel</span>
          </div>
          <div class="col ">
            <a href="/artikel/create" type="button" class="btn btn-primary float-end">Tambah Artikel</a>
          </div>
      </div>
    </div>
    <div class="card-body">

        @foreach ($artikel as $item)
        <div class="card mb-3">
            <div class="card-body">
                <h3>{{ $item->judul }}</h3>
                <p>{{ Carbon\Carbon::parse($item->created_at)->format('d F Y') }}</p>
                <hr />
                <form action="/artikel/{{ $item->id }}" method="POST">
                    <div class="d-grid">
                        <div class="btn-group">
                            <a href="/artikel/{{ $item->id }}" class="btn btn-primary">Lihat Selengkapnya</a>
                            @csrf
                            @method('DELETE')

                            <button type="submit" class="btn btn-danger">Delete</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>            
        @endforeach
    </div>
  </div>
@endsection