@extends('layout/main')

@section('judul', 'Detail')

@section('content')
<nav aria-label="breadcrumb" class="mt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/artikel">Home</a></li>
      <li class="breadcrumb-item"><a href="/artikel">Artikel</a></li>
      <li class="breadcrumb-item active" aria-current="page">Detail</li>
    </ol>
</nav>

<div class="row">
    <div class="col-12 col-md-8">
        <div class="card mb-3">
            <div class="card-body">
                <h3>{{ $artikel->judul }}</h3>
                <p>{{ Carbon\Carbon::parse($artikel->created_at)->format('d F Y') }}</p>
                <hr />
                <p>{!! $artikel->konten !!}</p>
                <div class="d-grid">
                    <a href="/artikel/{{ $artikel->id }}/edit" class="btn btn-success">Edit Artikel</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col col-md">
        <div class="card">
            <div class="card-body">
                <h3>Kategori</h3>
                @foreach ($kategori as $item)
                    <span class="badge bg-success">{{ $item->nama }}</span>
                @endforeach
                <hr />
                <div class="d-grid">
                    <a href="/artikel/addKategori/{{ $artikel->id }}" class="btn btn-success">Tambah Kategori</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection