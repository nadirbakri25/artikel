@extends('layout/main')

@section('judul', 'Tambah Data')

@section('content')
<nav aria-label="breadcrumb" class="mt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/artikel">Home</a></li>
      <li class="breadcrumb-item"><a href="/kategori">Kategori</a></li>
      <li class="breadcrumb-item active" aria-current="page">Edit Kategori</li>
    </ol>
</nav>

<form method="POST" action="/kategori/{{ $kategori->id }}">
    @csrf
    @method('PUT')

    <input type="hidden" class="form-control" id="inputId" name="id" value="{{ $kategori->id }}">

    <div class="mb-3">
        <label for="inputNama" class="form-label">Nama</label>
        <input type="text" class="form-control" id="inputNama" name="nama" value="{{ $kategori->nama }}">
        @error('nama')
            <p class="text-danger">{{ $message }}</p>
        @enderror
    </div>

    <button type="submit" class="btn btn-primary">Submit</button>
</form>
@endsection