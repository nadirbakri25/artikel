@extends('layout/main')

@section('judul', 'Kategori')

@section('content')
<nav aria-label="breadcrumb" class="mt-3">
    <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="/artikel">Home</a></li>
      <li class="breadcrumb-item active" aria-current="page">Kategori</li>
    </ol>
</nav>

<div class="card">
    <div class="card-header">
      <div class="row align-items-center">
          <div class="col-10">
              <span>Kategori</span>
          </div>
          <div class="col">
            <a href="/kategori/create" type="button" class="btn btn-primary float-end">Tambah Kategori</a>
          </div>
      </div>
    </div>
    <div class="card-body">
        <div class="row justify-content-center text-center">
            @foreach ($kategori as $item)    
            <div class="col-4">
                <div class="card mt-2 mb-2">
                    <h4>{{ $item->nama }}</h4>
                    <a class="stretched-link" href="/kategori/filter/{{ $item->id }}"></a>
                    <form action="/kategori/{{ $item->id }}" method="POST">
                        <div class="d-grid">
                            <div class="btn-group">
                                @csrf
                                @method('DELETE')
                                
                                <a href="/kategori/{{ $item->id }}/edit" type="button" class="btn btn-sm btn-primary">Edit</a>
                                <button type="submit" class="btn btn-sm btn-danger">Hapus</button>                        
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endforeach
        </div>
    </div>
  </div>
@endsection