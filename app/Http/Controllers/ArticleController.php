<?php

namespace App\Http\Controllers;

use App\Models\Article;
use App\Models\ArticleCategory;
use App\Models\Category;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $artikel = Article::all();

        return view ('artikel/index', compact('artikel'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kategori = Category::all();

        return view ('artikel/create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul'       => 'required|max: 255',
            'category_id' => 'required|numeric|not_in:0',
            'konten'      => ['required']
        ]);

        Article::create([
            'judul'       => $request->judul,
            'konten'      => $request->konten
        ]);

        $artikel = Article::where('konten', '=', $request->konten)->first();

        ArticleCategory::create([
            'category_id' => $request->category_id,
            'article_id'  => $artikel->id
        ]);

        return redirect('/artikel');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $artikel = Article::find($id);
        $kategori = Article::find($id)->categories()->get();

        if(!$artikel) {
            abort(404);
        }
        
        return view ('artikel/show', compact('artikel', 'kategori'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artikel = Article::find($id);

        if(!$artikel) {
            abort(404);
        }

        return view ('artikel/edit', compact('artikel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'judul'       => 'required|max: 255',
            'konten'      => 'required'
        ]);

        Article::find($id)->update([
            'judul'       => $request->judul,
            'konten'      => $request->konten
        ]);

        return redirect('/artikel');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ArticleCategory::where('article_id', '=', $id)->delete();

        Article::find($id)->delete();
        return redirect('/artikel');
    }

    public function cari(Request $request)
    {
        $artikel = Article::where('judul','like',"%".$request->cari."%")->get();

        return view ('artikel/index', compact('artikel'));
    }

    public function addKategori($id)
    {
        $artikel = Article::find($id);
        $kategori = Article::find($id)->categories()->get();
        $idKategori = collect([]);

        foreach ($kategori as $item) {
            $idKategori->push($item->id);
        }

        $kategoriExclude = Category::whereNotIn('id', $idKategori)->get();

        return view ('artikel.addKategori', compact('kategoriExclude','artikel'));
    }

    public function storeKategori(Request $request)
    {
        $request->validate([
            'category_id' => 'required|numeric|not_in:0',
        ]);

        ArticleCategory::create([
            'category_id' => $request->category_id,
            'article_id'  => $request->id
        ]);

        return redirect('/artikel');
    }
}
